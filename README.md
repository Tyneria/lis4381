> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381 Mobile Web Application Development

## Tyneria Cobbins

LIS4381 Requirements

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Ampps
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android app
    - Provide screenshots of completed app
    - Provide Screenshots of Skillsets 1-3

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create ERD based upon business rules
    - Provide screenshot of completed ERD
    - Provide DB Resource Links
    - Provide Screenshots of Skillsets 4-6

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create a launcher icon image and display it in both activities (screens) 
    - Must add background color(s) to both activities 
    - Must add border around image and button 
    - Must add text shadow (button)
    - Provide Screenshots of App Interfaces
    - Provide Screenshots of Skillsets 7-9


5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Test JQuery Validations
    - Add regular expressions per entity attribute requirements
    - Link to local lis4381 web app
    - Provide screenshot of passed validation
    - Provide screenshot of failed validation
    - Provide Screenshots of Skillsets 10-12

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Test JQuery Validations
    - Test Server-Side Validations
    - Test Client Side Validations
    - Provide Screenshot of Index.php
    - Provide Screenshots of Failed and Passed Validations
    - Provide Screenshots of Skillsets 13-15

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Test JQuery Validations
    - Test PHP Server-Side Validations 
    - Link a RSS Feed
    - Provide Screenshot of Index.php
    - Provide Screenshots of Failed and Passed Validations
    