> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS 4381

## Tyneria Cobbins

### Assignment #3 Requirements:

*Sub-Heading:*

1. Create ERD based upon business rules
2. Provide screenshot of completed ERD
3. Provide DB Resource Links

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running application�s opening user interface
* Screenshot of running application�s processing user input
* Screenshots of 10 records for each table

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of ERD*:

![A3 ERD](img/erd.png)

*Screenshot of running application�s opening user interface*:

![A3 app 1](img/app1.png)

*Screenshot of running application�s processing user input*:

![A3 app 2](img/app2.png)

*Screenshot of 10 records for each table*:

![A3 ERD data](img/table1.png)
![A3 ERD data](img/table2.png)
![A3 ERD data](img/table3.png)

*Screenshot of Skillset 4*:

![Skillset 4 Screenshot](img/ss4.png)

*Screenshot of Skillset 5*:

![Skillset 5 Screenshot](img/ss5.png)

*Screenshot of Skillset 6*:

![Skillset 6 Screenshot](img/ss6.png)

####Links:

*A3 MWB Link:*
[A3 MWB Link](docs/a3.mwb)

*A3 SQL Link:*
[A3 SQL Link](docs/a3.sql)
