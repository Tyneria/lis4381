> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Tyneria Cobbins

### Assignment #5 Requirements:

*Sub-Heading:*

1. Test JQuery Validations
2. Test Server-Side Validations
3. Test Client Side Validations
4. Link to Local lis4381 Web App
5. Provide Screenshot of Index.php
6. Provide Screenshots of Failed and Passed Validations

#### README.md file should include the following items:

* Display A5 Index/Main Page
* Provide screenshot of failed validation
* Provide screenshot of passed validation

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
. 

#### Assignment Screenshots:

*Screenshot of Main Page*:

![Main Page Screenshot](img/index.png)

*Screenshot of Invalid Validation*:

![Invalid Validation Screenshot](img/invalid.png)

*Screenshot of Valid Validation*:

![Valid Validation Screenshot](img/valid.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passed.png)

*Screenshot of Error Page*:

![Error Page Screenshot](img/error.png)

*Screenshot of Skillset 13*:

![Skillset 13 Screenshot](img/ss13.png)

*Screenshot of Skillset 14*:

![Skillset 14 Screenshot](img/ss14.png)

![Skillset 14 Screenshot](img/ss14_2.png)

*Screenshot of Skillset 15*:

![Skillset 15 Screenshot](img/ss15.png)



*Link to local lis4381 web app:*
[Link to local lis4381 web app](http://localhost/repos/lis4381/index.php)

