> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS4381

## Tyneria Cobbins

### Assignment #2 Requirements:

*Sub-Heading:*

1. Create Healthy Recipes Android app
2. Provide screenshots of completed app
3. Complete Chapter Questions (Chs 3, 4)

#### README.md file should include the following items:

* Screenshot of Bruschetta Recipe 
* Screenshot of completed app
* 


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of App First Page*:

![App First Page Screenshot](img/app1.png)

*Screenshot of App Second Page*:

![App Second Page Screenshot](img/app2.png)

*Screenshot of Skillset 1*:

![Skillset 1 Screenshot](img/ss1.png)

*Screenshot of Skillset 2*:

![Skillset 2 Screenshot](img/ss2.png)

*Screenshot of Skillset 3*:

![Skillset 3 Screenshot](img/ss3.png)




#### Tutorial Links:

