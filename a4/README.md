> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Tyneria Cobbins

### Assignment #4 Requirements:

*Sub-Heading:*

1. Test JQuery Validations
2. Add regular expressions per entity attribute requirements
3. Link to local lis4381 web app

#### README.md file should include the following items:


* Display A4 Index/Main Page
* Provide screenshot of failed validation
* Provide screenshot of passed validation

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Main Page*:

![Main Page Screenshot](img/main.png)

*Screenshot of Failed Validation*:

![Failed Validation Screenshot](img/fail.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/pass.png)

*Screenshot of Skillset 10*:

![Skillset 10 Screenshot](img/ss10.png)

*Screenshot of Skillset 11*:

![Skillset 11 Screenshot](img/ss11.png)

*Screenshot of Skillset 12*:

![Skillset 12 Screenshot](img/ss12.png)



*Link to local lis4381 web app:*
[Link to local lis4381 web app](http://localhost/repos/lis4381/index.php)

