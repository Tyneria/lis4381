> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Tyneria Cobbins

### Project #1 Requirements:

*Sub-Heading:*

1. Create a launcher icon image and display it in both activities (screens) 
2. Must add background color(s) to both activities 
3. Must add border around image and button 
4. Must add text shadow (button)

#### README.md file should include the following items:

* Screenshot of running application�s first user interface
* Screenshot of running application�s second user interface



> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*App Interface 1*:

![First User Interface](img/app1.png)

*App Interface 2*:

![Second User Interface](img/app2.png)

*App Interface 3:

![Third User Interface](img/app3.png)

*Screenshot of Skillset 7*:

![Skillset 7 Screenshot](img/ss7.png)

*Screenshot of Skillset 8*:

![Skillset 8 Screenshot](img/ss8.png)

*Screenshot of Skillset 9*:

![Skillset 9 Screenshot](img/ss9.png)

