> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Tyneria Cobbins

### Project #2 Requirements:

*Sub-Heading:*

1. RSS Feed
2. Test Server-Side Validations
3. Server side edit/delete functionality
4. Link to Local lis4381 Web App
5. Provide Screenshot of Index.php
6. Provide Screenshots of Failed and Passed Validations

#### README.md file should include the following items:

* Display P2 Index/Main Page
* Provide screenshot of failed validation
* Provide screenshot of passed validation
* Screenshot of RSS Feed

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
. 

#### Assignment Screenshots:

*Screenshot of Main Page*:

![Main Page Screenshot](img/home.png)

*Screenshot of Index Page*:

![Index Page Screenshot](img/index.png)

*Screenshot of Invalid Edit*:

![Edit Page Screenshot](img/edit.png)

*Screenshot of Error*:

![Error Page Screenshot](img/error.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passed.png)

*Screenshot of Delete Prompt Page*:

![Delete Prompt Screenshot](img/delete.png)

*Screenshot of Delete Page*:

![Delete Page Screenshot](img/delsuccess.png)

*Screenshot of Feed*:

![Feed Screenshot](img/feed.png)









*Link to local lis4381 web app:*
[Link to local lis4381 web app](http://localhost/repos/lis4381/index.php)

